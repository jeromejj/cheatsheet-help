<?php
/**
 * Created by PhpStorm.
 * User: Jerome.JAQUEMET
 * Date: 08.01.2019
 * Time: 10:31
 */


//region variables globales
$timestamp = strtotime("today");
//$timestamp = strtotime("2019-1-8");
//$timestamp = time();

//endregion


//region initialisation
$daysOfWeek = array(
    1 => "Mo",
    2 => "Tu",
    3 => "We",
    4 => "Th",
    5 => "Fr",
    6 => "Sa",
    7 => "Su",
);

//endregion


//region entry point

//endregion


//region functions

//endregion

?>

<!-- region gabarit -->
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="doc/style.css">
    </head>
    <body>

        <script src="doc/jquery-1.11.1.min.js"></script>
        <script src="doc/javascript.js"></script>
    </body>
</html>








